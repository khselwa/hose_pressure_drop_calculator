# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 14:24:30 2023

@author: kai.selwa

HOSE PRESSURE DROP CALCULATOR
Uses the Darcy-Weisbach equation to calculate pressure drop in circular pipe

**disclaimer: I am barely an engineer**

Aassumptions/Notes:
========================
    1) surface roughness equivalent smoothed rubber (0.1 mm)
    2) 50/50 water/glycol coolant
    3) 20 degC coolant only; future version of tool could include variable temperature inputs
    4) Math checked with http://www.pressure-drop.com/Online-Calculator/
    5) Used this script to calculate friction factors: https://github.com/robmarkoski/D-Arcy-Weisbach-Friction-Factor-Calculation/blob/master/d-friction.py
    
Inputs:
========================
    l (in)     
    d (in)
    Q (lpm)   
    roughness(mm)   *defaults to smoothed rubber: .01 mm

"""
import math

def dwcalc(d, l, Q, roughness=0.01):
    print("About to do this math!")
    
    #initialize default rho & dynamic viscosity @20 degC
    rho = 998.206 #kg/m^3
    dynamic_viscosity = .00100161 #Pa*s
    
    #convert units
    d = d/39.3701 #in to m
    l = l/39.3701 #in to m
    Q = Q/(60*1000) #Lpm to m^3/s

    #calculate and print Reynolds Number
    A = 3.14159*(d/2)**2 #Pipe Area in m^2
    v = float(Q/A) #Flow Velocity in m/s
    reynolds = v*d*rho/dynamic_viscosity # Reynolds Number for Full Circular Pipe
    print("Reynolds Number: " + str(reynolds))
    
    if reynolds <= 2000:
        print("Laminar Flow")
    elif reynolds <= 4000 and reynolds >2000:
        print("Critical Flow")
    else:
        print("Turbulent Flow")
        
    #calculate friction factor via Colebrook-White and Swamee Jain equations
    def cwfcalc(d,roughness,reynolds):
        friction = 0.08 #Starting Friction Factor
        while 1:
            leftF = 1 / friction**0.5 #Solve Left side of Eqn
            rightF = - 2 * math.log10(2.51/(reynolds * friction**0.5)+(roughness/1000)/(3.72*d)) # Solve Right side of Eqn
            friction = friction - 0.000001 #Change Friction Factor
          #  print(leftF)
          #  print(rightF)
          #  print(friction)
            if (rightF - leftF <= 0): #Check if Left = Right
                break
        return friction
    
    def sjfcalc(d, roughness, reynolds):
        return 0.25 / (math.log10((roughness/1000)/(3.7*d)+5.74/(reynolds**0.9)))**2
    
    
    cwf = cwfcalc(d, roughness, reynolds)
    #print ("Colebrooke-White Friction: " + str(cwf))
    sjf = sjfcalc(d, roughness, reynolds)
    #print("Swamee Jain Friction: " +str(sjf))


    #calculate pressure drop via Darcy-Weisbach equation
    deltaP = (cwf * l * rho * (v**2)) / (2 * d)
    print ("Pressure drop (kPa): " + str(deltaP/1000))
    return deltaP/1000
    #print("Pressure Drop (Pa): " +str(deltaP))
    #print("Pressure Drop (bar): " +str(deltaP/100000))
    

# you can uncomment and adjust the following line to use the tool:
# dwcalc (0.750, 120, 30)
    